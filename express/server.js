const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// Parse reqs of content-type application/json
app.use(bodyParser.json());

// Parse reqs of content type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Simple route
app.get("/", (req, res) => {
  res.json({ message: "Leadbot dashboard API server" });
});

// Listing routes
require("./app/routes/listing.routes")(app);

// Setting port to listen for reqs
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.` );
});