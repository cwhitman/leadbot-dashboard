module.exports = app => {
  const listings = require("../controllers/listing.controller");

  // Retrieve all listings
  app.get("/listings", listings.findAll);

  // Retrieve listing by id
  app.get("/listings/:listingId", listings.findOne);

  // Update listing by id (used for blacklist)
  app.put("/listings/:listingId", listings.blacklistListing);

  // Update listing by id (used for blacklist)
  app.put("/listings/hide/:listingId", listings.hideById);
};