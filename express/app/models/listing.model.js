const listingDb = require("./listingDb");

// Constructor
const Listing = function(listing) {
  this.posted = listing.posted;
  this.date = listing.date;
  this.title = listing.title;
  this.link = listing.link;
  this.state_country = listing.state_country;
  this.location = listing.location;
  this.category = listing.category;
  this.search_term = listing.search_term;
  this.cl_id = listing.cl_id;
  this.show_flag = listing.show_flag;
}

const Blacklist = function(blacklist) {
  this.title = blacklist.title;
  this.cl_id = blacklist.cl_id;
}

Listing.getAll = (titleSearch, result) => {
  var dbQuery = titleSearch ? `SELECT * FROM listings WHERE title LIKE '%${titleSearch}%' AND show_flag = 1 ORDER BY posted DESC` : "SELECT * FROM listings WHERE show_flag = 1 ORDER BY posted DESC";

  listingDb.query(dbQuery, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Listings: ", res.length);
    result(null, res);
  });
};

Listing.getById = (listingId, result) => {
  listingDb.query(`SELECT * FROM listings WHERE id = ${listingId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("Found listing: ", res[0]);
      result(null, res[0]);
      return;
    }

    // No listing with id
    result(null, res[0]);
  });
};

Listing.hideById = (listingId, result) => {
  listingDb.query(`UPDATE listings SET show_flag = 0 where id = ${listingId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      console.log(res);
      return;
    }

    console.log("Hid listing with id = " + listingId)
    console.log(res);
    result(null, res);
  })
}

Listing.blacklistById = (listingId, result) => {
  listingDb.query(
    `SELECT * FROM listings WHERE id = ${listingId}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.affectedRows == 0 || !res.length) {
        // Listing with id not found
        result({ kind: "not_found" }, null);
        return;
      }

      let listingId = res[0].id;
      let title = res[0].title;
      let cl_id = res[0].cl_id;
      let show_flag = res[0].show_flag;

      console.log("Found listing." + '\n' + "ID: ", listingId + '\n' + "Title: " + title + '\n' + "CLID: " + cl_id + '\n' + "Show Flag/Blacklist: " + show_flag);
      
      listingDb.query(`UPDATE listings SET show_flag = 0 where id = ${listingId}`, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }

        if (res.affectedRows == 0) {
          // Listing not found with the id
          result({ kind: "not_found" }, null);
          return;
        }

        listingDb.query(`INSERT INTO blacklist SET title="${title}", cl_id=${cl_id}`, (err, res) => {

          if (err) {

            if (err.code === 'ER_DUP_ENTRY') {
              console.log(`Item with CL_ID: ${cl_id} and Title: "${title}" is already in the blacklist`);
              result({ kind: "dup_entry" }, null);
              return;
            }
            
            console.log("Error: " + '\n' + err);
            result(err, null);
            return;
          }

          console.log("Added item to blacklist: ", {id: res.insertId});

          // Add like items to blacklist
          listingDb.query(`INSERT INTO blacklist (title, cl_id) SELECT title, cl_id FROM listings WHERE title LIKE "%${title}%" ON DUPLICATE KEY UPDATE cl_id = VALUES(cl_id)`, (err, res) => {
            if (err) {
              console.log("error: ", err);
              result(err, null);
              return;
            }

            listingDb.query(`UPDATE listings SET show_flag = 0 WHERE title LIKE "%${title}%"`, (err, res) => {
              if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
              }

              console.log("Listings: ", res);
            })

            console.log("Listings: ", res);
          })

          result(null, {id: res.insertId});
        });
      });
      return;
    }
  );
};

module.exports = Listing;