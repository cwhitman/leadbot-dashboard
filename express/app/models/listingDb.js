const mysql = require("mysql2");
const dbConfig = require("../config/db.config");

// Create DB connection
const connection = mysql.createConnection({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB
});

connection.connect(error => {
  if (error) throw error;
  console.log("Successful database connection");
});

module.exports = connection;