const Listing = require("../models/listing.model");

// Retrieve all listings
exports.findAll = (req, res) => {
  Listing.getAll(req.query.search, (err, data) => {
    if (err) {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving listings."
      });
    }
    else res.send(data);
  });
};

// Find one listing by id
exports.findOne = (req, res) => {
  Listing.getById(req.params.listingId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Listing with id = ${req.params.listingId} was not found.`
        });
      } else {
        res.status(500).send({
          message: "error retrieving listing with id " + req.params.listingId
        });
      }
    } else res.send(data);
  });
};

exports.hideById = (req, res) => {
  // Validate
  if (!req.body) {
    res.status(400).send({
      message: "Hide request cannot be empty!"
    });
  }

  Listing.hideById(req.params.listingId, (err, data) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: "Error retrieving listing with id " + req.params.listingId
      });
    } else res.send(data);
  });
};

// Mark listing for blacklist
exports.blacklistListing = (req, res) => {
  // Validate
  if (!req.body) {
    res.status(400).send({
      message: "Blacklist request cannot be empty!"
    });
  }

  Listing.blacklistById(
    req.params.listingId,
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Could not find listing with id ${req.params.listingId}.`
          });
        } if (err.kind === "dup_entry") {
          res.status(500).send({
            message: `This listing is already in the blacklist.`
          });
        } else {
          res.status(500).send({
            message: "Error updating listing with id " + req.params.listingId
          });
        }
      } else res.send(data);
    }
  );
};