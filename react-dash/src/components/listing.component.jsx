import React, { Component } from "react";
import ListingDataService from "../services/listing.service";
import { Link } from "react-router-dom";

export default class Listing extends Component {
  constructor(props) {
    super(props);
    this.getListing = this.getListing.bind(this);
    this.blacklistListing = this.blacklistListing.bind(this);

    this.state = {
      currentListing: {
        id: null,
        title: "",
        show_flag: 1
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getListing(this.props.match.params.id);
  }

  getListing(id) {
    ListingDataService.get(id)
      .then(response => {
        this.setState({
          currentListing: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  blacklistListing() {
    ListingDataService.blacklist(this.state.currentListing.id)
    .then(response => {
      console.log(response.data);
      this.props.history.push('/listings')
    })
    .catch(e => {
      console.log(e);
    });
  }

  render() {
    const { currentListing } = this.state;

    return (
      <div>
        {currentListing ? (
          <div>
            <h4>Listing Detail</h4>
            <div>
              <label>
                <strong>Title: </strong>
              </label>{" "}
              <p>{currentListing.title}</p>
            </div>
            <div>
              <label>
                <strong>Posted: </strong>
              </label>{" "}
              <p>{currentListing.date}</p>
            </div>
            <div>
              <label>
                <strong>Search Term: </strong>
              </label>{" "}
              <p>{currentListing.search_term}</p>
            </div>
            <div>
              <label>
                <strong>Location: </strong>
              </label>{" "}
              {currentListing.location}, {currentListing.state_country}
            </div>
            <div>
              <label>
                <strong>Link: </strong>
              </label>{" "}
              <a
              href={currentListing.link}
              className="btn"
              >
                View Listing
              </a>
            </div>
            <button
              className="btn btn-danger"
              onClick={this.blacklistListing}
            >
              Blacklist
            </button>
          </div>
        ) : (
          <div>
            <br />
            <p>Select listing for details</p>
          </div>
        )}
      </div>
    )
  }
}