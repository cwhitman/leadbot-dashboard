import React, { Component } from "react";
import ListingDataService from "../services/listing.service";
import { Link } from "react-router-dom";

export default class ListingsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveListings = this.retrieveListings.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveListing = this.setActiveListing.bind(this);
    this.searchTitle = this.searchTitle.bind(this);
    this.blacklistListing = this.blacklistListing.bind(this);
    this.hideListing = this.hideListing.bind(this);

    this.state = {
      listings: [],
      currentListing: null,
      currentIndex: -1,
      searchTitle: ""
    };
  }

  componentDidMount() {
    this.retrieveListings();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle
    });
  }

  retrieveListings() {
    ListingDataService.getAll()
      .then(response => {
        this.setState({
          listings: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveListings();
    this.setState({
      currentListing: null,
      currentIndex: -1
    });
  }

  setActiveListing(listing, index) {
    this.setState({
      currentListing: listing,
      currentIndex: index
    });
  }

  searchTitle() {
    ListingDataService.findByTitle(this.state.searchTitle)
      .then(response => {
        this.setState({
          listings: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  blacklistListing() {
    ListingDataService.blacklist(this.state.currentListing.id)
    .then(response => {
      console.log(response.data);
      this.refreshList();
    })
    .catch(e => {
      console.log(e);
    });
  }

  hideListing() {
    ListingDataService.hide(this.state.currentListing.id)
    .then(response => {
      console.log(response.data);
      this.refreshList();
    })
    .catch(e => {
      console.log(e);
    });
  }

  render() {
    const { searchTitle, listings, currentListing, currentIndex } = this.state;

    return (
      <div className="list">
        <div className="row">
        <div className="col-md-12">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search by title"
              value={searchTitle}
              onChange={this.onChangeSearchTitle}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.searchTitle}
              >
                Search
              </button>
            </div>
          </div>
        </div>
        </div>
        <div className="container row">
        <div className="col">
          <h4>Lead Listings</h4>

          <ul className="list-group">
            {listings &&
              listings.map((listing, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveListing(listing, index)}
                >
                  <strong>{listing.date}</strong> - {listing.title}
                </li>
              ))
            }
          </ul>
        </div>
        <div className="col-4 order-2 card">
          {currentListing ? (
            <div className="sticky-top py-3">
              <h4>Listing Detail</h4>
              <div>
                <label>
                  <strong>Title: </strong>
                </label>{" "}
                <p>{currentListing.title}</p>
              </div>
              <div>
                <label>
                  <strong>Posted: </strong>
                </label>{" "}
                <p>{currentListing.date}</p>
              </div>
              <div>
                <label>
                  <strong>Search Term: </strong>
                </label>{" "}
                <p>{currentListing.search_term}</p>
              </div>
              <div>
                <label>
                  <strong>Location: </strong>
                </label>{" "}
                {currentListing.location}, {currentListing.state_country}
              </div>
              <div>
                <a
                  href={currentListing.link}
                  className="btn btn-info"
                  target="_blank"
                >
                  View Listing
                </a>
                <Link
                  to={"/listings/" + currentListing.id}
                  className="btn btn-primary"
                >
                  Details
                </Link>
                <button
                  className="btn btn-danger"
                  onClick={this.hideListing}
                >
                  Hide
                </button>
                <button
                  className="btn btn-danger"
                  onClick={this.blacklistListing}
                >
                  Blacklist
                </button>
              </div>
            </div>
          ) : (
            <div>
              <br />
              <p>Select listing for details</p>
            </div>
          )}
        </div>
        </div>
      </div>
    )
  }
}