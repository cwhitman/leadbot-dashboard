import http from "../http-common";

class ListingDataService {
  getAll() {
    return http.get("/listings");
  }

  findByTitle(title) {
    return http.get(`/listings?search=${title}`);
  }

  get(id) {
    return http.get(`/listings/${id}`);
  }

  blacklist(id) {
    return http.put(`/listings/${id}`);
  }

  hide(id) {
    return http.put(`/listings/hide/${id}`);
  }
}

export default new ListingDataService();