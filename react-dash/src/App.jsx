import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';

import Listing from "./components/listing.component";
import listingsList from "./components/listings-list.component";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <a href="/listings" className="navbar-brand">
              Leadbot
            </a>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/listings"} className="nav-link">
                  Listings
                </Link>
              </li>
            </div>
          </nav>

          <div className="container mt-3">
            <Switch>
              <Route exact path={["/", "/listings"]} component={listingsList} />
              <Route path="/listings/:id" component={Listing} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;